package kz.codeforces.mywebapp;

import kz.codeforces.mywebapp.database.Database;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class BaseServlet extends HttpServlet {

    public static  final String ATTR_TMS = "tms";
    public static final String ATTR_PLRS = "players";
    public static  final String ATTR_TRNMNT = "tournament";

    // Способ доступа к Database через поле
    protected Database db = new Database();
    // Способ доступа к Database через Getter
    // Можно использовать любой из них, но советуют Getter
    public Database getDatabase() {
        return db;
    }

    @Override
    public void init() throws ServletException {
        //Запуск драйвера для работы с базой данных
        try {
            db.connect();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    // Отключение драйвера
    @Override
    public void destroy() {
        try {
            db.disconnect();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void forward(HttpServletRequest request, HttpServletResponse response, String pageName) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/" + pageName);
        dispatcher.forward(request,response);
    }
}
