
package kz.codeforces.mywebapp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MyServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("### INSIDE doGet");
        
        String brg = req.getParameter("burger");
        String chs = req.getParameter("cheese");
        String tmt = req.getParameter("tomato");
        
        PrintWriter writer = resp.getWriter();
        writer.println("<table  Style=\"width: 400px; padding: 4px; border: 2px solid black;\">\n" +
"                    <tr>\n" +
"                        <td>Your order</td>\n" +
"                        <td>Check</td>\n" +
"                    </tr>\n" +
"                    <tr>\n" +
"                        <td>Burger</td>\n" +
"                        <td>"+brg+"</td>\n" +
"                    </tr>\n" +
"                    <tr>\n" +
"                        <td>Cheese</td>\n" +
"                        <td>"+chs+"</td>\n" +
"                    </tr>\n" +
"                    <tr>\n" +
"                        <td>Tomato</td>\n" +
"                        <td>"+tmt+"</td>\n" +
"                    </tr>\n" +
"            </table>");
        
        /*
        PrintWriter writer = resp.getWriter();
        if(loginValue.equals("Artur") && passValue.equals("123")) {
            writer.println("<h1>Authorizated</h1>");
            writer.println("<h3>Your name: " + loginValue + "</h3>");
            writer.println("<h3>Your password: " + passValue + "</h3>");
        } else {
            writer.println("<h1>Not authorized!!!</h1><br>" + "User: " + loginValue + " not exist!!!" + "<br><h1>Wrong password: " + passValue + "</h1>");
        }
        */
        
        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
    
    

    
    
    
    
}
