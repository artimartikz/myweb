package kz.codeforces.mywebapp;

import kz.codeforces.mywebapp.models.Player;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@WebServlet(name = "PlayersServlet", urlPatterns = {"/players", "/players/delete", "/players/edit"})
public class PlayersServlet extends BaseServlet {

    private final String PARAM_PLAYER_NAME = "playerName";
    public static final String ATTR_PLAYER = "player";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        System.out.println("### Player Action = " + action);

        if (request.getServletPath().equals("/players/delete")) {
            doDelete(request, response);
        } else if (request.getServletPath().equals("/players/edit")) {
            doPut(request, response);
        } else {
            String name = request.getParameter(PARAM_PLAYER_NAME);
            try {
                db.getPlayerDao().createPlayer(name);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        response.sendRedirect("/players");

//        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getServletPath().equals("/players/edit")) {
            String id = request.getParameter("id");
            System.out.println("### Edit Player by ID " + "(" + id + ")");

            try {
                Player player = db.getPlayerDao().getPlayerById(Integer.parseInt(id));
                request.setAttribute(ATTR_PLAYER, player);

                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/edit-player.jsp");
                dispatcher.forward(request, response);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            List<Player> players = new ArrayList<>();

            try {
                players = db.getPlayerDao().getAllPlayers();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            request.setAttribute(ATTR_PLRS, players);

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/players.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Enumeration<String> ids = req.getParameterNames();

        while (ids.hasMoreElements()) {
            String idName = ids.nextElement();

            String[] parts = idName.split("_");
            int id = Integer.parseInt(parts[1]);

            System.out.println("### Player's ID: " + id);

            try {
                db.getPlayerDao().deletePlayerByID(id);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String playerName = req.getParameter("playerName");
        System.out.println("### doPut Player name: " + playerName + " id: " + id);

        try {
            db.getPlayerDao().updatePlayer(Integer.parseInt(id), playerName);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
