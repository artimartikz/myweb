package kz.codeforces.mywebapp;

import kz.codeforces.mywebapp.models.Player;
import kz.codeforces.mywebapp.models.PlayerStats;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "StatisticsServlet", urlPatterns = {"/statistics"})
public class StatisticsServlet extends BaseServlet {

    public static final String ATTR_STATS = "ATTR_STATS";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<PlayerStats> stats = new ArrayList<>();

        try {
            stats = db.getAllPlayerStats();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        request.setAttribute(ATTR_STATS, stats);

        forward(request, response, "statistics.jsp");
    }
}
