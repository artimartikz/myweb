package kz.codeforces.mywebapp;

import kz.codeforces.mywebapp.models.Player;
import kz.codeforces.mywebapp.models.Team;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@WebServlet(name = "TeamServlet", urlPatterns = {"/teams", "/teams/delete"})
public class TeamServlet extends BaseServlet {

    private final String PARAM_TEAM_NAME = "teamName";
    public static final String ATTR_TEAM = "team";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        System.out.println("Team Action = " + action);

        if (request.getServletPath().equals("/teams/delete")) {
            doDelete(request, response);
        } else if (request.getServletPath().equals("/teams/edit")) {
            doPut(request, response);
        } else {
            String teamName = request.getParameter(PARAM_TEAM_NAME);
            String playerA = request.getParameter("playerAName");
            String playerB = request.getParameter("playerBName");

            try {
                db.createTeam(teamName, playerA, playerB);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        response.sendRedirect("/teams");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Player> players = new ArrayList<>();
        List<Team> teams = new ArrayList<>();

        try {
                players = db.getPlayerDao().getAllPlayers();
                teams = db.getAllTeams();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute(ATTR_PLRS, players);
        request.setAttribute(ATTR_TMS, teams);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/teams.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Enumeration<String> ids = req.getParameterNames();
        while (ids.hasMoreElements()) {
            String idName = ids.nextElement();

            String[] parts = idName.split("_");
            int id = Integer.parseInt(parts[1]);

            System.out.println("Team's ID: " + id);

            try {
                db.deleteTeamByID(id);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String teamName = req.getParameter("playerName");
        String playerAName = req.getParameter("playerAName");
        String playerBName = req.getParameter("playerBName");
        System.out.println("### doPut Player name: " + teamName + " id: " + id);

        try {
            db.updateTeam(Integer.parseInt(id), teamName);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
