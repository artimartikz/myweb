package kz.codeforces.mywebapp;

import kz.codeforces.mywebapp.models.Team;
import kz.codeforces.mywebapp.models.Tournament;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@WebServlet(name = "TournamentServlet",urlPatterns = {"/tournaments", "/tournaments/delete"})
public class TournamentServlet extends BaseServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        System.out.println("Tournament Action = " + action);

        if (request.getServletPath().equals("/tournaments/delete")) {
            doDelete(request, response);
        } else {
            String tournamentName = request.getParameter("tournamentName");
            String teamA = request.getParameter("teamA");
            String teamB = request.getParameter("teamB");

            try {
                db.createTournament(tournamentName, teamA, teamB);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        response.sendRedirect("/tournaments");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Team> teamsList = new ArrayList<>();
        List<Tournament> tournamentsList = new ArrayList<>();

        try {
            teamsList = db.getAllTeams();
            tournamentsList = db.getAllTournaments();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute(ATTR_TRNMNT,tournamentsList);
        request.setAttribute(ATTR_TMS,teamsList);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/tournaments.jsp");
        dispatcher.forward(request,response);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Enumeration<String> ids = req.getParameterNames();
        while (ids.hasMoreElements()) {
            String idName = ids.nextElement();

            String[] parts = idName.split("_");
            int id = Integer.parseInt(parts[1]);

            System.out.println("Tournament's ID: " + id);

            try {
                db.deleteTournamentByID(id);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
