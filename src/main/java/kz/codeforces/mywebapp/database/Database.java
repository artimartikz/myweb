package kz.codeforces.mywebapp.database;

import kz.codeforces.mywebapp.models.Player;
import kz.codeforces.mywebapp.models.PlayerStats;
import kz.codeforces.mywebapp.models.Team;
import kz.codeforces.mywebapp.models.Tournament;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database {

    protected final String DB_DRIVER = "org.postgresql.Driver";
    protected final String DB_URL = "jdbc:postgresql://localhost:5432/tournament";
    protected final String DB_USER = "postgres";
    protected final String DB_PASSWORD = "qwerty123";

    private Connection conn;

    private PlayerDao playerDao;

    //Подключение к базе данных
    public void connect() throws ClassNotFoundException, SQLException {
        Class.forName(DB_DRIVER);
        conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        playerDao = new PlayerDao(conn);
    }

    public PlayerDao getPlayerDao() {
        return playerDao;
    }

    //Отключение от базы данных
    public void disconnect() throws SQLException {
        conn.close();
    }

    public List<Team> getAllTeams() throws SQLException {
        List<Team> teams = new ArrayList<>();

        Statement stmtTeam = conn.createStatement();
        ResultSet resultSet1 = stmtTeam.executeQuery("select  * from teams");
        while (resultSet1.next()) {
            String name = resultSet1.getString("name");
            String playerA = resultSet1.getString("player_a");
            String playerB = resultSet1.getString("player_b");
            int id = resultSet1.getInt("id");

            Team team = new Team(name, playerA, playerB, id);
            team.setId(id);
            teams.add(team);
        }
        stmtTeam.close();
        return teams;
    }

    public List<Tournament> getAllTournaments() throws SQLException {
        List<Tournament> tournamentsList = new ArrayList<>();

        Statement stmtTourn = conn.createStatement();
        ResultSet resultSetTourn = stmtTourn.executeQuery("select * from tournaments");
        while (resultSetTourn.next()) {
            String name = resultSetTourn.getString("name");
            String teamA = resultSetTourn.getString("team_a");
            String teamB = resultSetTourn.getString("team_b");
            int id = resultSetTourn.getInt("id");

            tournamentsList.add(new Tournament(name, teamA, teamB, id));
        }
        stmtTourn.close();
        return tournamentsList;
    }

    public void createTeam(String teamName, String playerA, String playerB) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("insert into teams(name, player_a, player_b) values (?, ?, ?)");
        statement.setString(1, teamName);
        statement.setString(2, playerA);
        statement.setString(3, playerB);
        statement.executeUpdate();
        statement.close();
    }

    public void createTournament(String tournamentName, String teamA, String teamB) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("insert into tournaments(name, team_a, team_b) values(?, ?, ?)");
        statement.setString(1, tournamentName);
        statement.setString(2, teamA);
        statement.setString(3, teamB);
        statement.executeUpdate();
        statement.close();
    }

    public void deleteTeamByID(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("delete from teams where id = ?");
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
    }

    public void deleteTournamentByID(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("delete from tournaments where id = ?");
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
    }

    public void updateTeam(int id, String name) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("update teams set name = ? where id = ?");
        statement.setString(1, name);
        statement.setInt(2, id);
        statement.executeUpdate();
        statement.close();
    }

    public  List<PlayerStats> getAllPlayerStats() throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("select s.*, p.name, (games - wins) as diff \n" +
                "from stats as s, players as p\n" +
                "where s.player_id = p.id\n" +
                "order by s.player_id");

        ResultSet resultSet = stmt.executeQuery();

        ArrayList<PlayerStats> stats = new ArrayList<>();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String playerName = resultSet.getString("name");
            int player_id = resultSet.getInt("player_id");

            int games = resultSet.getInt("games");
            int wins = resultSet.getInt("wins");
            int diff = resultSet.getInt("diff");

            stats.add(new PlayerStats(id, playerName, player_id, games, wins, diff));
        }
        stmt.close();
        return stats;
    }
}
