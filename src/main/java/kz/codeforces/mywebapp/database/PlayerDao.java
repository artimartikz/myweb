package kz.codeforces.mywebapp.database;

// DAO class Data Access Object

import kz.codeforces.mywebapp.models.Player;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerDao {

    public static final String TABLENAME = "players";

    private final String SQL_GET_PLAYER = "select * from " + TABLENAME +" where id = ?";
    private final String SQL_GET_ALL_PLAYERS = "select * from " + TABLENAME +" order by id";

    private Connection conn;

    public PlayerDao(Connection conn) {
        this.conn = conn;
    }

    public Player getPlayerById(int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(SQL_GET_PLAYER);
        stmt.setInt(1, id);

        ResultSet resultSet = stmt.executeQuery();
        Player player = null;

        if (resultSet.next()) {
            player = getPlayer(resultSet);
        }
        stmt.close();
        return player;
    }

    public List<Player> getAllPlayers() throws SQLException {
        ArrayList<Player> players = new ArrayList<>();
        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery(SQL_GET_ALL_PLAYERS);

        while (resultSet.next()) {
            players.add(getPlayer(resultSet));
        }
        stmt.close();
        return players;
    }

    public void createPlayer(String playerName) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("insert into players(name) values(?)");
        statement.setString(1, playerName);
        statement.executeUpdate();
        statement.close();
    }

    public void deletePlayerByID(int id) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("delete from players where id = ?");
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
    }

    public void updatePlayer(int id, String name) throws SQLException{
        PreparedStatement statement = conn.prepareStatement("update players set name = ? where id = ?");
        statement.setString(1, name);
        statement.setInt(2, id);
        statement.executeUpdate();
        statement.close();
    }

    private Player getPlayer(ResultSet resultSet) throws SQLException {
        int dbid = resultSet.getInt("id");
        String name = resultSet.getString("name");

        return new Player(dbid, name);
    }

}
