package kz.codeforces.mywebapp.models;

// POJO class Plain Old Java Object

public class PlayerStats {
    int id;
    String playerName;
    int player_id;

    int games;
    int wins;
    int diff;

    public PlayerStats(int id, String playerName, int player_id, int games, int wins, int diff) {
        this.id = id;
        this.playerName = playerName;
        this.player_id = player_id;
        this.games = games;
        this.wins = wins;
        this.diff = diff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(int player_id) {
        this.player_id = player_id;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDiff() {
        return diff;
    }

    public void setDiff(int diff) {
        this.diff = diff;
    }
}
