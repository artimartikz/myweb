package kz.codeforces.mywebapp.models;

// POJO class Plain Old Java Object

public class Team {

    int id;
    String name;
    String PlayerA;
    String PlayerB;

    public Team(String name, String playerA, String playerB, int id) {
        this.id = id;
        this.name = name;
        PlayerA = playerA;
        PlayerB = playerB;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlayerA() {
        return PlayerA;
    }

    public void setPlayerA(String playerA) {
        PlayerA = playerA;
    }

    public String getPlayerB() {
        return PlayerB;
    }

    public void setPlayerB(String playerB) {
        PlayerB = playerB;
    }
}
