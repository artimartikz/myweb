package kz.codeforces.mywebapp.models;

// POJO class Plain Old Java Object

public class Tournament {

    private String name;
    private String teamA;
    private String teamB;
    private int id;

    public Tournament(String name, String teamA, String teamB, int id) {
        this.name = name;
        this.teamA = teamA;
        this.teamB = teamB;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamA() {
        return teamA;
    }

    public void setTeamA(String teamA) {
        this.teamA = teamA;
    }

    public String getTeamB() {
        return teamB;
    }

    public void setTeamB(String teamB) {
        this.teamB = teamB;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
