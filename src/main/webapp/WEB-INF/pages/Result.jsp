
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>Result Page</h1>

<table>
    <tr>
        <th>name</th>
        <th>value</th>
    </tr>

    <% for(int i = 0; i<10; i++) { %>
    <tr>
        <td><%= i%></td>
        <td>${i}</td>
    </tr>
    <% } %>

</table>
</body>
</html>
