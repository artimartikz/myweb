<%@ page import="kz.codeforces.mywebapp.PlayersServlet" %>
<%@ page import="kz.codeforces.mywebapp.models.Player" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% Player player = (Player) request.getAttribute(PlayersServlet.ATTR_PLAYER); %>

<html>
<head>
    <title>Edit Player</title>
</head>
<body>

<form action="/players/edit" method="post">
    <input type="text" name="playerName" value="<%= player.getName() %>"/>
    <input type="text" name="id" value="<%= player.getId() %>" hidden/>
    <input type="submit" value="save">
</form>
</body>
</html>
