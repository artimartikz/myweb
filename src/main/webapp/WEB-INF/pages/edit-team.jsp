<%@ page import="kz.codeforces.mywebapp.PlayersServlet" %>
<%@ page import="kz.codeforces.mywebapp.models.Player" %>
<%@ page import="kz.codeforces.mywebapp.models.Team" %>
<%@ page import="kz.codeforces.mywebapp.TeamServlet" %><%--
  Created by IntelliJ IDEA.
  User: TourAiTravel
  Date: 15.08.2020
  Time: 23:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% Player player = (Player) request.getAttribute(PlayersServlet.ATTR_PLAYER); %>
<% Team team = (Team) request.getAttribute(TeamServlet.ATTR_TEAM); %>

<html>
<head>
    <title>Edit Team</title>
</head>
<body>

<form action="/teams/edit" method="post">
    <input type="text" name="teamName" value="<%= team.getName() %>"/>
    <input type="text" name="id" value="<%= team.getId() %>" hidden/>
    <input type="submit" value="save">


    <input type="text" name="playerAName" value="<%= team.getPlayerA() %>"/>
    <input type="text" name="id" value="<%= player.getId() %>" hidden/>
    <input type="submit" value="save">

    <input type="text" name="playerBName" value="<%= team.getPlayerB() %>"/>
    <input type="text" name="id" value="<%= player.getId() %>" hidden/>
    <input type="submit" value="save">
</form>


</body>
</html>
