<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.codeforces.mywebapp.models.Player" %>
<%@ page import="kz.codeforces.mywebapp.PlayersServlet" %>
<%@ page import="static kz.codeforces.mywebapp.PlayersServlet.ATTR_TMS" %>
<%@ page import="java.util.List" %>
<%@ page import="static kz.codeforces.mywebapp.BaseServlet.ATTR_PLRS" %>
<%
    List<Player> list = (List<Player>) request.getAttribute(ATTR_PLRS);
%>

<!doctype html>
<html lang="en">

<%@include file="../templates/head.html" %>

<body>
<%@include file="../templates/nav.html" %>

<div class="container-fluid">
    <div class="row">

        <%@include file="../templates/sidebarmenu.html" %>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

            <h2>Players</h2>

            <form method="post" action="players/delete">
                <div class="table-responsive">
                    <table class="table table-striped table-sm">

                        <thead>
                        <tr>
                            <th>Edit</th>
                            <th>#</th>
                            <th>Name</th>
                            <th># games</th>
                            <th># wins</th>
                            <th># losses</th>
                        </tr>
                        </thead>

                        <tbody>

                        <% for (int i = 0; i < list.size(); i++) { %>
                        <tr>
                            <td>
                                <form>
                                    <input type="text" name="id" value="<%= list.get(i).getId() %>" hidden>
                                    <input type="submit" value="Edit" formaction="players/edit" formmethod="get" >
                                </form>
                            </td>
                            <td><input type="checkbox" name="playerid_<%= list.get(i).getId() %>"></td>
                            <td><%= list.get(i).getName() %>
                            </td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                        <% } %>

                        </tbody>

                    </table>
                </div>

                <div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Delete player</button>
                </div>
            </form>

            <br>

            <form class="card p-2 col-sm-4" action="players" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Player name" name="playerName">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">Add player</button>
                    </div>
                </div>
            </form>
    </main>
</div>
</div>

<%@include file="../templates/scripts.html" %>

</body>


</html>
