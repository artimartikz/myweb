<%@ page import="kz.codeforces.mywebapp.StatisticsServlet" %>
<%@ page import="java.util.List" %>
<%@ page import="kz.codeforces.mywebapp.PlayersServlet" %>
<%@ page import="kz.codeforces.mywebapp.models.PlayerStats" %>
<html lang="en">

<%@include file="../templates/head.html" %>

<body>
<%@include file="../templates/nav.html" %>

<% List<PlayerStats> stats = (List<PlayerStats>) request.getAttribute(StatisticsServlet.ATTR_STATS);  %>

<div class="container-fluid">
    <div class="row">
        <%@include file="../templates/sidebarmenu.html"%>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>Statistics</h2>

            <table border="1">
<%--               Table head --%>
                <thead>
                <tr>
                    <td>Player ID</td>
                    <td>Player Name</td>
                    <td>Games Played</td>
                    <td>Wins</td>
                    <td>Difference</td>
                </tr>
                </thead>
<%--                Table Body--%>
                <tbody>
                <% for (PlayerStats ps: stats) { %>
                <tr>
                    <td>
                        <%= ps.getPlayer_id() %>
                    </td>
                    <td>
                        <%= ps.getPlayerName() %>
                    </td>
                    <td>
                        <%= ps.getGames() %>
                    </td>
                    <td>
                        <%= ps.getWins() %>
                    </td>
                    <td>
                        <%= ps.getDiff() %>
                    </td>
                </tr>
                <% } %>
                </tbody>

            </table>



        </main>
    </div>
</div>

<%@include file="../templates/scripts.html" %>

</body>


</html>

