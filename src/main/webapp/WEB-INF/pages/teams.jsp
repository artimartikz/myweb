<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.codeforces.mywebapp.models.Player" %>
<%@ page import="kz.codeforces.mywebapp.TeamServlet" %>
<%@ page import="kz.codeforces.mywebapp.PlayersServlet" %>
<%@ page import="kz.codeforces.mywebapp.models.Team" %>
<%@ page import="java.util.List" %>
<%
    List<Team> list = (List<Team>) request.getAttribute(TeamServlet.ATTR_TMS);
    List<Player> playersList = (List<Player>) request.getAttribute(TeamServlet.ATTR_PLRS);

%>

<!doctype html>
<html lang="en">

<%@include file="../templates/head.html" %>

<Body>
<%@include file="../templates/nav.html" %>


<div class="container-fluid">
    <div class="row">

        <%@include file="../templates/sidebarmenu.html"%>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

            <h2>Teams</h2>

            <form method="post" action="teams/delete">
                <div class="table-responsive">
                    <table class="table table-striped table-sm">

                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>#</th>
                        <th>Team Name</th>
                        <th>Player A</th>
                        <th>Player B</th>
                    </tr>
                    </thead>

                    <tbody>
                    <% for (int i = 0; i < list.size(); i++) { %>
                    <tr>
                        <td>
                            <form method="get">
                                <input type="text" name="id" value="<%= list.get(i).getId() %>" hidden>
                                <input type="submit" value="Edit" formaction="teams/edit" >
                            </form>
                        </td>
                        <td><input type="checkbox" name="teamid_<%= list.get(i).getId() %>"></td>
                        <td><%= list.get(i).getName() %></td>
                        <td><%= list.get(i).getPlayerA() %></td>
                        <td><%= list.get(i).getPlayerB() %></td>
                    </tr>
                    <% } %>
                    </tbody>

                </table>
                    <br>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Delete team</button>
                </div>
            </form>

            <br>

                <form class="card p-2 col-sm-4" action="teams" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Add a team name" name="teamName">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">Add team</button>
                        </div>
                    </div>
                <br>

                <h5>Choose your player A</h5>
                <select class="custom-select d-block w-100" name="playerAName">
                    <% for (Player player : playersList) {%>
                    <option><%= player.getName() %></option>
                    <% } %>
                </select>
                <br>

                <h5>Choose your player B</h5>
                <select class="custom-select d-block w-100" name="playerBName">
                    <% for (Player player : playersList) {%>
                    <option><%= player.getName() %></option>
                    <% } %>
                </select>
                </form>
        </main>
    </div>
</div>

<%@include file="../templates/scripts.html" %>

</body>


</html>
