<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.codeforces.mywebapp.models.Tournament" %>
<%@ page import="kz.codeforces.mywebapp.models.Team" %>
<%@ page import="kz.codeforces.mywebapp.TournamentServlet" %>
<%@ page import="java.util.List" %>
<%
    List<Tournament> tournamentList = (List<Tournament>) request.getAttribute(TournamentServlet.ATTR_TRNMNT);
    List<Team> teamsList = (List<Team>) request.getAttribute(TournamentServlet.ATTR_TMS);
%>
<!doctype html>
<html lang="en">
<%@include file="/WEB-INF/templates/head.html" %>
<body>
<%@include file="/WEB-INF/templates/nav.html" %>
<div class="container-fluid">
    <div class="row">
        <%@include file="/WEB-INF/templates/sidebarmenu.html" %>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>Tournaments</h2>

            <form method="post" action="tournaments/delete">
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tournament</th>
                            <th>Team A</th>
                            <th>Team B</th>
                        </tr>
                        </thead>

                        <tbody>
                        <% for (int i = 0; i < tournamentList.size(); i++) {
                            Tournament t = tournamentList.get(i);
                        %>

                        <tr>
                            <td><input type="checkbox" name="tournamentid_<%= t.getId() %>">
                            </td>
                            <td><%=t.getName()%>
                            </td>
                            <td><%=t.getTeamA()%>
                            </td>
                            <td><%=t.getTeamB()%>
                            </td>
                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Delete tournament</button>
                </div>
            </form>
            <br>
            <form class="card p-2 col-sm-4" action="tournaments" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Tournament name" name="tournamentName">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">Add Tournament</button>
                    </div>
                </div>
                <br>

                <h5>Choose Team A</h5>
                <select class="custom-select d-block w-100" name="teamA">
                    <% for (Team team : teamsList) {%>
                    <option><%= team.getName() %>
                    </option>
                    <% } %>
                </select>
                <br>

                <h5>Choose Team B</h5>
                <select class="custom-select d-block w-100" name="teamB">
                    <% for (Team team : teamsList) {%>
                    <option><%= team.getName() %>
                    </option>
                    <% } %>
                </select>

            </form>

            </form>
    </div>
    </main>
</div>
</div>

<%@include file="/WEB-INF/templates/scripts.html" %>

</body>
</html>

