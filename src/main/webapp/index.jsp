<!doctype html>
<html lang="en">

<%@include file="WEB-INF/templates/head.html" %>

<body>
<%@include file="WEB-INF/templates/nav.html" %>

<div class="container-fluid">
    <div class="row">

        <%@include file="WEB-INF/templates/sidebarmenu.html"%>


    </div>
</div>

<%@include file="WEB-INF/templates/scripts.html" %>

</body>


</html>
